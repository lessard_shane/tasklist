import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

var storage_cache = 'tasks'

try {
	let storage = JSON.parse(localStorage.getItem(storage_cache))
	if (typeof storage !== 'object' || !storage){
		throw 'cache not an object'
	}
}
catch(e){
	console.log(e)
	localStorage.setItem(storage_cache, '{"first list": []}')
}

export default new Vuex.Store({
	state: {
		lists: localStorage.getItem(storage_cache) ? JSON.parse(localStorage.getItem(storage_cache)) : {}
	},
	mutations: {
		add(state, item) {
			state.lists[item.list].push({task: item.task, complete: false})
		},
		addList(state, list) {
			if (!state.lists[list]){
				let newobj = {}
				newobj[list] = []
				state.lists = {...state.lists, ...newobj}
			}
		},
		removeList(state, list) {
			if (state.lists[list]){
				let lists = {}
				Object.keys(state.lists).forEach(item => {
					if (item !== list){
						lists[item] = state.lists[item]
					}
				})
				state.lists = Object.assign({}, lists)
			}
		},
		toggle(state, item) {
			state.lists[item.list][item.index].complete = !state.lists[item.list][item.index].complete
		},
		purge(state){
			Object.keys(state.lists).forEach(key => {
				state.lists[key] = state.lists[key].filter(item => !item.complete)
			})
		},
		reset(state){
			state.lists = {}
		}
	},
	actions: {
		add({commit, dispatch}, item){
			commit('add', item)
			dispatch('store')
		},
		addList({commit, dispatch}, list){
			commit('addList', list)
			dispatch('store')
		},
		removeList({commit, dispatch}, list){
			commit('removeList', list)
			dispatch('store')
		},
		toggle({commit, dispatch}, item){
			commit('toggle', item)
			dispatch('store')
		},
		purge({commit, dispatch}){
			commit('purge')
			dispatch('store')
		},
		reset({commit, dispatch}){
			commit('reset')
			dispatch('store')
		},
		store({state}){
			localStorage.setItem(storage_cache, JSON.stringify(state.lists))
		}
	},
	getters: {
		lists(state) {
			return state.lists
		},
	},
})
